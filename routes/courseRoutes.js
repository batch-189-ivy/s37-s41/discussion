const express = require('express');
const router = express.Router();
const auth = require("../auth");

const courseController = require('../controllers/courseController');

//Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	//const isAdminData = auth.decode(req.headers.authorization).isAdmin
	const isAdminData = userData.isAdmin

	if(isAdminData) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send("User is not an admin")

	}
})


//Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});

//Route for retrieving all active courses
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving a specific course
//localhost:4000/courses/62c3b6c89935079e40f26492
//courseId = 62c3b6c89935079e40f26492
// "/:parameterName"
router.get("/:courseId", (req, res) => {

	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

//Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});


router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});




module.exports = router;


