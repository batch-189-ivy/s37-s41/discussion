const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req, res) => {
	//Provides the user's ID for the getProfile controller method

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)
	console.log(userData);
	/*
		{
		  id: '62c28a34491fb81d5cb300c1',
		  email: 'bond@mail.com',
		  isAdmin: false,
		  iat: 1656985367
		}

	*/
	console.log(req.headers.authorization)


	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})


router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});





//Allows us to export
module.exports = router;
