const Course = require('../models/Course');

//Create a new course
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}


//Retrieve All Courses
module.exports.getAllCourses = async (data) => {
	
	if (data.isAdmin) {
		return Course.find({}).then(result => {
		return result
	})

	} else {
		res.send("Not an Admin")
	}
}


module.exports.getAllActive = (data) => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
}


module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}




module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})


}
